---
name: Adição de pacote.
about: Adicionem esse pacote!
title: ''
labels: adição
assignees: ''

---

**Nome do pacote:** `Pacote`

**Versão:** `1.10-exemplo`

**É necessária compilação?** `Não. O pacote está disponível em formato deb no site, porem não existe nos repositórios | Não. Porém o pacote não está disponível no formato deb. | Sim. O pacote necessita compilação e porte para o formato deb.`
