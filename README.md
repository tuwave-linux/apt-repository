# Repositório do Tuwave Jaguar
Este é o repositório  `apt`  do  **Tuwave Linux**, que comporta alguns pacotes que são interessantes para o usuário final, caso ache que será interessante incluir algum pacote, abra uma [Issue](https://github.com/Tuwave-Linux/repository/issues).

<details>
  <summary>lista de pacotes:</summary>

- code (Visual Studio Code 1.59.1)
- discord (Discord 0.0.15)
- google-chrome-stable (Google Chrome 92.0.4515.159-1)
- node-lts (NodeJS Versão LTS 14.17.5)
- node-current (NodeJS versão atual 16.8.0)
- linux-headers-5.10.60-xanmod1 (Headers do kernel Xanmod LTS 5.10.60)
- linux-image-5.10.60-xanmod1 (Kernel Xanmod LTS 5.10.60)
- vivaldi-stable 4.1 (Vivaldi 4.1)
- opera-stable 78.0 (Opera 78.0)
- ppfetch (Ppfetch 1.09)
  
  ---
  </details>

Adicione esse repositório no seu sistema com os seguintes comandos:
<details>
  <summary>Descontinuado...</summary>
  
```sh
echo 'deb [trusted=yes] https://sourceforge.net/projects/tuwave/files/repo core main' | sudo tee /etc/apt/sources.list.d/tuwave.list
```  
```sh
curl -s -L https://sourceforge.net/projects/tuwave/files/repo/tuwave.key/ | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/tuwave.gpg
```
</details>

---
__Esta versão é a versão final, portanto, o projeto estará arquivado e não será mais mantido.__

